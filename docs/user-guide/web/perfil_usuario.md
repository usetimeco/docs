# Actualizar datos de Perfil

En la opción de perfil, el usuario podrá modificar algunos de sus datos, tales como:

* Foto de Perfil
* Cargo
* Genero
* Teléfono Móvil y de oficina
* Dirección
* Cantidad de horas que labora a la semana


# Cambiar contraseña

En esta opción el usuario puede cambiar su clave, este campo esta validado para que solamente la persona que conozca la clave actual del usuario, pueda cambiar la clave, mediante la petición de la contraseña actual.

# Cerrar sesión

Esta opción cerrará la sesión de usuario, luego de esto el usuario sera dirigido a la ventana de iniciar sesión de la plataforma.

Esta sección, permite la descarga de Usetime en versión escritorio, donde hay 3 opciones:

![Crear cita](/img/Descargar.jpg)

![Crear cita](/img/Descargar_opciones.jpg)

Mediante estas aplicaciones, el usuario podrá registrar su tiempo trabajado y a qué lo dedica.

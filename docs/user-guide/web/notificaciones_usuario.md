En este botón aparecerán las notificaciones globales de los usuarios, aqui estos podrán ver:

* Invitaciones a Proyectos o tareas/entregables
* Creaciones de tareas dentro de un proyecto o tarea propio del usuario
* Cambios realizados a un proyecto o tarea
* Subida de documentos y creación de citas, comentarios en proyectos y tareas

![Crear cita](/img/Notificacion.jpg)

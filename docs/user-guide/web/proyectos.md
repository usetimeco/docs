En proyectos se encuentran opciones para crear y supervisar el proceso de un negocio, se presenta como trabajar un proyecto desde la plataforma de Usetime.


# Panel de Proyectos
![Panel de proyectos](/img/panel_pro.jpg)

En cada proyecto, se podrá ver la información , desde su creador, tiempo invertido en el proyecto, tareas, y estados de las tareas. Se visualiza de 2 formas, por listado o por mosaico.

# Creación de Proyectos
![Crear Proyecto](/img/crea_pro.jpg)

En esta opción, el usuario podrá crear un proyecto según los siguientes criterios:

-Asignarle un nombre al proyecto.

-Darle una Descripción de la finalidad de dicho proyecto.

-Darle una fecha de inicio y de finalización del proyecto.

<h4>Desde el  panel de proyectos al darle clic en "Ver proyecto", se mostrará una ventana donde hay una sección que muestra el contenido de cada proyecto, como las tareas que contiene, y unas funciones descritas a continuación : </h4>

![Crear Proyecto](/img/pro_info.jpg)

# Información del proyecto:

En esta área podemos encontrar los siguientes items de un proyecto:

## Ficha técnica
![Crear Proyecto](/img/f_tecnica.jpg)

En esta área se puede actualizar, Archivar o eliminar un proyecto, además contiene una ventana informativa sobre el proyecto:
![Crear Proyecto](/img/f_tecnica1.jpg)

## Gantt
![Crear Proyecto](/img/Gantt.jpg)

Este botón mostrará un resumen de proyectos, [tareas](tareas_y_entregables.md) y subtareas (incluyendo entregables), representados en un Gantt donde evidencia las duraciones del proyecto y las tareas que hay asignadas en función del tiempo:

![Crear Proyecto](/img/Gantt1.jpg)


## Documentos
![Crear Proyecto](/img/Docs.jpg)

En esta opción el usuario podrá ver en un listado todos los documentos que se han anexado al proyecto desde todas las tareas (También hay una opción en estas vistas para eliminar y subir nuevos archivos):

![Crear Proyecto](/img/Docs1.jpg)

## Linea de Tiempo
![Crear Proyecto](/img/timeline.jpg)

En esta opción se mostrará un diagrama en forma de línea de tiempo, donde el usuario que tenga acceso al proyecto podrá ver de manera cronológica y en secuencia lo que se ha realizado en el proyecto:

![Crear Proyecto](/img/timeline1.jpg)


## Tareas
![Crear Proyecto](/img/Tareas.jpg)

En esta opción, se podrán ver las tareas contenidas en el proyecto:

![Crear Proyecto](/img/tareas1.jpg)


## Invitación de Usuarios a Proyectos

Esta opción permite invitar usuarios a un proyecto, ya sea desde un grupo de trabajo en específico o terceros que se inviten a colaborar, asignándoles [permisos.](index.md#permisos-de-usuarios)


  [Sobre Usetime](../../about.md)

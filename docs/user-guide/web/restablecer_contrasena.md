

En la ventana de inicio de sesión, debajo de los campos de Email y Contraseña aparecerá una opción resaltada en azul que dice “Olvidó su contraseña?”, al dar clic en esta opción, el usuario será dirigido a un formulario con un campo donde deberá digitar su correo electrónico, luego de digitarlo ,aparecerá una notificación que informa al usuario, que ha sido enviado un correo para recuperar la contraseña.

Hay recordar que el usuario debe para poder restablecer una contraseña debe estar registrado para poder realizar esta acción y sobre todo que el correo que digite sea el que este contemplado en su registro, de lo contrario el usuario no podrá recibir el correo y por tanto se presentará un mensaje al usuario diciendo "El correo electrónico es inválido"

la siguiente imágen, muestra en un recuadro rojo, donde iniciar su procerso de restablecer contraseña
![Restablecer Contraseña](/img/ResCont.jpg)

En esta área de la plataforma el usuario podrá cambiar el idioma según su gusto, cualquiera las diferentes opciones de idioma que le ofrece Usetime como son:

![Cambiar Idioma](/img/Idioma.jpg)

* Inglés
* Español

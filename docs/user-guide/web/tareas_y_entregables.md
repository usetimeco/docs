En esta sección hablaremos de como se realizan procesos y el control de ellos, trabajando en 2 puntos principales:

| Proceso | Descripción  |
| :------------- | :------------- |
| Tarea       | Es un proceso, donde hay varias funciones como creación de documentos, y hechos que se miden por medio de tiempo agregado a este tipo de proceso.   |
|Entregable| En un proceso, en el cual se debe subir un documento/archivo, con la finalidad de poder validarlo luego de revisión.|

# Creación de tareas y entregables

Para crear tareas y entregables en la vista de un proyecto, el usuario debe digitar en el espacio que dice "Creación de Tarea", el nombre de la nueva tarea o entregable.

![Crear Proyecto](/img/crea_t_y_e.jpg)

Luego de digitar el nombre, el usuario debe dar clic en el botón crear y así, una nueva tarea será creada, pero si activa la casilla al lado del botón crear, se generará un nuevo entregable.

![Crear Proyecto](/img/crea_entregable.jpg)

## Panel de administración de tareas y entregables
![Crear Proyecto](/img/admin_panel_tareas.jpg)

Es el esquema donde se puede gestionar, administrar y observar toda la información relacionada con una tarea o entregable.

# Creación de Sub-tareas y Sub-entregables
![Crear Proyecto](/img/Crea_sub_t_e.jpg)

Es el mismo procedimiento, pero este se efectúa dentro del panel de administración de la tarea o entregable.

## Estados de las tareas y entregables
Los estados definen como se encuentra una Tareas y/o entregable
![Crear Proyecto](/img/Estado_tarea.jpg)

| ESTADO  | DESCRIPCIÓN     |
| :------------- | :------------- |
|Vencida     | Es cuando la tarea o entregable ha sobrepasado el tiempo que se le asignó para ser concretada  |
|En Poceso| Es toda tarea que se encuentra en el tiempo establecido al momento de su creación|
|Entregada | Es toda tarea o entregable sobre la que ha terminado su ejecución, y esta lista para ser validada |
|Validada|Es cuando una tarea es revisada y finalmente validada por el administrador(es) de la tarea|

## Actualización de datos de Tareas y entregables
![Crear Proyecto](/img/datos_tarea.jpg)

En esta opción el usuario puede actualizar la tarea, esta actualización se podrá realizar en torno a 3 criterios

* Cambiar nombre
* Agregar una descripción para la tarea
* Asignarle duración a la tarea por fecha y horas de duración.

## Invitación de usuarios a tareas y entregables

A diferencia de la invitación a proyectos la invitación a tareas y entregables, permite trabajar y ver tareas en específico, si una persona solo es agregada a una tarea, no podrá ver las demas tareas que posee el proyecto; entre las actividades que un usuario puede ejercer en tareas y entregables por tipo de permiso se encuentran las siguientes.

* Permisos para ver:
Un usuario con este permiso puede:
    * Ver el contenido de la tarea o entregable
    * Descargar documentos
    * Realizar comentarios en una tarea o entregable

* Permisos para gestionar
Un usuario con este permiso, además de los permisos del usuario ver, puede:
    * Agregar tiempo a una tarea para poder finalizarla
    * Subir Documentos para finalizar un entregable
    * Terminar tareas y entregables

* Permisos para administrador: Un usuario que sea administrador de una tarea o entregable, posee todos los permisos de los otros usuarios, además puede:

    * Invitar usuarios y asignarles permisos a tareas y/o entregables
    * Crear subtareas y subentregables
    * Subir documentos
    * Validar Tareas y entregables que se encuentren terminados.


## Subir/Descargar documentos
![Crear Proyecto](/img/doc_tareas.jpg)

Esta opción permite subir documentación concerniente a una tarea o entregable en específico, aquí el usuario selecciona un archivo que puede servir de guía para la realización de una tarea asignada y subirlo como documentación importante, igualmente permite descargas de documentos que se encuentren relacionados a la tarea.


## Hacer comentarios
![Crear Proyecto](/img/comentario.jpg)
Esta  opción sirve como herramienta de información de cada tarea o entregable, donde cada usuario sin importar que  permisos tenga, realice anotaciones sobre la gestión de cada tarea o entregable



## Gestión de tareas

En esta parte, un usuario puede agregar tiempo de trabajo en una tarea, para poder finalizarla y/o validarla, según el rol que tenga el usuario, si no asigna tiempo, no se podrá terminar la tarea.

![Crear Proyecto](/img/gestion_tarea.jpg)


## Gestion de entregables

El usuario antes de terminar un entregable, deberá subir un documento que contenga lo que se le ha solicitado, solamente cuando realice esta acción, este podrá terminar el entregable y dependiendo de su rol podrá validarlo o no.

![Crear Proyecto](/img/gestion_entreg.jpg)

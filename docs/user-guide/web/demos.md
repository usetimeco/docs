Para crear una cuenta de prueba en Usetime, debe dirigirse a la ventana de inicio de sesión, cuando se encuentre en esa ventana, al costado izquierdo podrá visualizar dos botónes que dicen:

![Crear cita](/img/Demo.jpg)

* Sitio Web
* Demo

luego el usuario, debe dar clic en "Demo" y la plataforma lo dirigirá a un formulario donde este podrá gestionar su propia cuenta de prueba.

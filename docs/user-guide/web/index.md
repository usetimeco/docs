# Funciones plataforma web

Nuestra plataforma permite que un usuario pueda gestionar su tiempo, sin importar que sea de manera remota o no, mostrándole un sin fin de oportunidades, como:

* Medición de productividad a nivel personal y por equipo de trabajo (empresa).
* Gestión de [proyectos](proyectos.md), mediante [tareas y entregables](tareas_y_entregables.md).
* Asignación de actividades por permisos específicos a cada usuario.
* Gestión por tiempos mediante diagramas de Gantt y lineas de tiempo que permiten ejercer control sobre las actividades de cada proyecto.


# Tipos de usuarios que maneja la plataforma

En la plataforma web existen:

* Roles de usuarios.
* Permisos de usuarios.

###  Roles de usuarios

Permiten que los usuarios puedan según su rol, hacer uso de las funciones de Usetime, los roles que aplican para los usuarios son:

| ROL DE USUARIO    | FUNCIONES QUE PUEDEN DESARROLLAR SOBRE LA PLATAFORMA    |
| :------------- | :------------- |
|<h3>Usuario Normal<h3/>|<ul> <li> Puede trabajar en el manejo personal de la cuenta (Contraseñas, perfil, etc.).</li> <li>Realizar acciones en calendario.</li> <li>Realizar descargas.</li><li>Crear proyectos.</li><li>Crear tareas y entregables.</li> </ul> </li>|
|<h3>Usuario de Reportes<h3/>|Adicional a las funciones de un usuario normal este puede: <ul><li>Ver reportes por aplicaciones y/o grupo de aplicaciones.</li><li>Ver reportes por diferentes criterios de filtrado.</li><li>Ver reportes de productividad, a nivel personal, empresarial o por grupo específico.</li><li>Ver reporte de acividad de proyectos.</li> </ul>|
|<h3>Usuario Administrador<h3/>|Adicional a las funciones de los otros usuarios, este puede: <ul><li>Crear, modificar y/o eliminar aplicaciones y grupos de aplicaciones.</li><li>Crear, modificar y/o eliminar Usuarios y grupos de Usuarios.</li><li>Asignar o modificar los permisos a usuarios.</li> </ul>|


### Permisos de Usuarios

![Asignar Permisos](/img/permisos.jpg)

Son el nivel de gestión que puede ejercer un usuario en [proyectos](proyectos.md), [tareas y entregables](tareas_y_entregables.md), se clasifican en tres tipos de permisos.

  * Permisos para ver:
      * En proyectos: El usuario solo podrá ver información de las tareas o entregables que tiene el proyecto.
      * En tareas y entregables: El usuario solo podrá ver el contenido de la tarea, descargar [documentos](tareas_y_entregables.md#subirdescargar-documentos) y realizar [comentarios](tareas_y_entregables.md#hacer-comentarios) en una tarea, o entregable.

  * Permisos para gestionar:
      * En proyectos: El usuario podrá crear [tareas](tareas_y_entregables.md#creacion-de-tareas-y-entregables) y [subtareas](tareas_y_entregables.md#creacion-de-sub-tareas-y-sub-entregables) dentro del proyecto.
      * En tareas y entregables: [Agregar tiempo a las tareas](tareas_y_entregables.md#gestion-de-tareas), [subir entregables](tareas_y_entregables.md#gestion-de-entregables), terminar tareas y entregables, además de todos los permisos del rol Ver.

  * Permisos para administrar:
      * En proyectos: Aparte de las opciones de los roles ver y gestionar, el admin puede invitar usuarios a trabajar a un proyecto.
      * En tareas y entregables: Contiene todos los permisos, incluyendo invitar usuarios, crear subtareas y subentregables, subir documentos, y validar tareas y entregables.

# Introducción al calendario

En el calendario de Usetime, el usuario podrá agendar sus diferentes citas y reuniones, solamente con dar clic en el espacio que el desee, desde cualquier vista que se encuentre.

El calendario tiene 3 tipos de vista e las cuales un usuario puede ver las diferentes citas que tiene agendadas en su calendario, estas vistas son por mes, día y semana.

# Creación de citas

Para agendar una cita, el usuario deberá, desde la vista donde se encuentre, dar clic en el espacio donde desee crear la cita, de ahí se desplegará un formulario en el que este debe diligenciar unos datos tales como:

* Nombre del evento
* Descripcíon (No es obligatorio diligenciarlo)
* Hora de inicio y de término del evento

![Crear cita](/img/Calendario.jpg)
